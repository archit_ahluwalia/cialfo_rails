class JsonPaginationController < ApplicationController
  def index                                                                     # with pagination controller
    
    @colg= Colg.all.paginate(page: params[:page], per_page: 10)                 # fetching from db
  end

  def load_db                                                                   # populate db controller
    file = File.read("public/temp1.json")
    
    list_college = JSON.parse(file)
    @i=0
    # abc=college_model.new
    count= Colg.distinct.count('name')                                          # to get the number of records so that it doesn't keep on over writing 
    
    if count==0
      list_college.each do |college_v|                                          # parsing through json to every record and saving it in db
        colg_model= Colg.new(college_v)
        colg_model.save  
        @i+=1
      end
    end  
    
  end
end
