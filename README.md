# README #

Ruby version: Ruby 2.2.1 . 
Rails version: Rails 4.2.4.

Scraping gem used: nokogiri(v:1.6.7.2).
pagination gem used: will_paginate(3.0).

* The Rake Task(cialfo:scrap_task) writes the data to public/temp1.json file.
* First run the task for the file to be created and then run the app to see the data.

For app:
* Populate db link is to fetch the data from file and put into the models which are further used for pagination.