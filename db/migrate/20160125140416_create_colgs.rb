class CreateColgs < ActiveRecord::Migration
  def change
    create_table :colgs do |t|
      t.string :name
      t.string :rank
      t.string :tuition_fees
      t.integer :total_enrollment
      t.string :acceptance_rate
      t.string :average_retention_rates
      t.string :graduation_rate

      t.timestamps null: false
    end
  end
end
