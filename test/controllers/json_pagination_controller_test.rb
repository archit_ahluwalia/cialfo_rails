require 'test_helper'

class JsonPaginationControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get load_db" do
    get :load_db
    assert_response :success
  end

end
