namespace :cialfo do
  desc "TODO"
  task scrap_task: :environment do
    require 'nokogiri'
    require 'open-uri'
    require 'json'

		i=0
		list_colg= Array.new
		url = "http://colleges.usnews.rankingsandreviews.com/best-colleges/rankings/national-universities/data"
		begin 																	# outer loop go over all the pages
			next_url=false
			doc = Nokogiri::HTML(open(url))
			table=doc.css('table.ranking-data tbody tr')
#			puts i

			table.each do |row|													#inner loop to go over all the rows in one page
				rank=row.css('td div span.rankscore-bronze').text				#extract the relevant data and strip white spaces 
				name=row.css('td.college_name a.school-name').inner_html.strip
				fee= row.css('td.search_tuition_display').first.text.strip
				roll= row.css('td.total_all_students').first.text.strip
				a_rate= row.css('td.r_c_accept_rate').first.text.strip
				a_r_rate= row.css('td.r_c_avg_pct_retention').first.text.strip
				g_rate = row.css('td.r_c_avg_pct_grad_6yr').first.text.strip
				college= Array.new

				college= [														# form the array for one particular college records
					"name" => name,
					"rank" => rank,
					"tuition_fees" => fee,
					"total_enrollment" => roll,
					"acceptance_rate" => a_rate,
					"average_retention_rates" => a_r_rate,
					"graduation_rate" => g_rate
				]

				list_colg.concat college										# append the current records to final array
			end
			last_ele=doc.css('p#pagination a.pager_link')
			if(last_ele.last.inner_html.strip == "Next »")						#if the link to the next page exists then go to the next page
				url= "http://colleges.usnews.rankingsandreviews.com" + doc.css('p#pagination a.pager_link').last.attr('href')
				next_url=true
				i+=1															#
			end

		end while next_url
		list_colg.to_json														#convert final array to json 
		
		File.open("public/temp1.json","w") do |f|								#writes to file
		f.write(JSON.pretty_generate(list_colg))
		end
	end
end
